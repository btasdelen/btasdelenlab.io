#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Bilal Taşdelen'
SITENAME = 'Bilal Taşdelen Personal Website'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'
PLUGIN_PATHS = ['plugins/pelican-bib',]
PLUGINS = ('pelican_youtube','pelican_bib')
TIMEZONE = 'US/Pacific'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('DISC', 'https://disc.usc.edu/'),
         ('MREL', 'https://mrel.usc.edu/'),
         ('e-mail', 'mailto:tasdelenATuscDOTedu'),)

# Social widget
SOCIAL = (('linkedin', 'https://www.linkedin.com/in/bilaltasdelen/'),
          ('github', 'https://github.com/btasdelen'),
          ('gitlab', 'https://gitlab.com/btasdelen'),)

DEFAULT_PAGINATION = False
# PAGINATED_TEMPLATES = ('categories', 'archives')

THEME = 'themes/Flex'

# Flex options
MAIN_MENU = False
SITETITLE = "Bilal Taşdelen"
SITESUBTITLE = " PhD. Candidate at University of Southern California, ECE Department"
HOME_HIDE_TAGS = True
ROBOTS = "index, follow"

THEME_COLOR = 'dark'
THEME_COLOR_AUTO_DETECT_BROWSER_PREFERENCE = True
THEME_COLOR_ENABLE_USER_OVERRIDE = True
PYGMENTS_STYLE = "monokai"

SITELOGO = "/images/profile.jpg"

CC_LICENSE = {
    'name': 'Creative Commons Attribution-ShareAlike',
    'version': '4.0',
    'slug': 'by-sa'
}

COPYRIGHT_YEAR = 2020

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

PUBLICATIONS_CUSTOM_STYLE = True
PUBLICATIONS_STYLE_ARGS = {'site_author': 'Bilal Tasdelen'}
