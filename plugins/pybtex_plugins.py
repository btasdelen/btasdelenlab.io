from pybtex.database import Person
from pybtex.style.formatting import unsrt, toplevel
from pybtex.style.template import (
    field, first_of, href, join, names, optional, optional_field, sentence,
    tag, together, words
)

class PelicanStyle(unsrt.Style):

    def __init__(self, site_author='', **kwargs):
        super().__init__(**kwargs)
        self.site_author = Person(site_author)

        # Allows to apply special formatting to a specific author.
        def format(person, abbr=False):
            if person == self.site_author:
                return tag('strong') [ self.name_style.format(person, abbr) ]
            else:
                return self.name_style.format(person, abbr)

        self.format_name = format

    def get_patent_template(self, e):
        template = toplevel [
            sentence [self.format_names('author')],
            self.format_title(e, 'title'),
            sentence [
                field('number'),
                optional[ unsrt.date ]
            ],
            self.format_web_refs(e),
        ]
        return template
