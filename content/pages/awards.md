Title: Awards & Honors
Date: 2020-05-30 10:20
Modified: 2010-12-05 19:30
Tags: awards
Slug: awards
Authors: Bilal Taşdelen
Summary: My Awards and Honors

* **Best Poster Award**, Graduate Research Conference, 2018, Ankara, Turkey.
* **1st Place Prize**, IEEE Turkey Senior Projects Competition, 2017, Cyprus.
* **Magna Cum Laude**, Bilkent University, Ankara, Turkey.
* **Comprehensive Scholarship**, Bilkent University, 2013-2017.
* **Ranked 124th among 1.2 million examinees**, University entrance examinations, Turkey, 2013.