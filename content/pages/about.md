Title: About Me
Date: 2020-05-30 10:20
Tags: about
Slug: about
save_as: index.html
Status: hidden
Authors: Bilal Taşdelen
Summary: Who am I

I am currently pursuing a PhD. degree from ECE department at University of Southern California (USC), under the supervision of Prof. [Krishna Nayak](https://sipi.usc.edu/~knayak/). I am a member of the [MREL](https://mrel.usc.edu/) and [DISC](https://disc.usc.edu/).

Previously, I got my Master's degree in Electrical and Electronics Engineering from Bilkent University, Ankara, Turkey, under the supervision of Prof. Ergin Atalar. I was also a research assistant at National Magnetic Resonance Research Center (UMRAM).

My academic background is magnetic resonance imaging, electromagnetic interference mitigation, and signal/image processing.
