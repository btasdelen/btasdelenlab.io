Title: Research and Technical Skills
Date: 2020-05-30 10:20
Modified: 2010-12-05 19:30
Tags: research
Slug: research
Authors: Bilal Taşdelen
Summary: My Research interests and technical skills

------------------
## Research Interests

* Medical Imaging
* Hardware Development
* Novel Imaging Techniques
* Signal Processing
* Biomedical Instrumentation

--------------------------------
## Technical and Personal Skills

* **Computer Skills:** 
    * **Programming Languages:** C, C++, Java, Python
    * **Scientific Programming:** Python (NumPy, SciPy), Matlab
    * Arduino, Raspberry Pi, Freedom KL46Z, etc.
    * LaTeX
    * VHDL

Can create GUIs in Qt Library (Python and C++). 
Gave Linux tutorials in a student branch.

* **Industry Software Skills:** 
    * **Circuit Simulation:** AWR Microwave Office, LTspice 
    * **PCB Prototyping:** KiCAD, Eagle
    * **CAD:** Autodesk Fusion360
    * **FPGA:** Xilinx ISE and Vivado 

* **Languages:** 
    * Turkish **(Native)**
    * English **(Advanced)** 
    * German **(Beginner)**
