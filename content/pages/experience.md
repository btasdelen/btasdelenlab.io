Title: Experience
Date: 2020-05-30 10:20
Modified: 2010-12-05 19:30
Tags: experience
Slug: experience
Authors: Bilal Taşdelen
Summary: My Work Experience History

# Research Experience

-----------------------------------------------------------
## Dynamic Imaging Science Center, Los Angeles

### *Research Assistant*

**2021-Ongoing**

[DISC Website](https://disc.usc.edu/)

-----------------------------------------------------------
## UMRAM (National Magnetic Resonance Research Center), Ankara

### *Researcher*

**2017-2021**

[UMRAM Website](http://umram.bilkent.edu.tr/)


# Teaching Experience

-----------------------------------------------------------
## Bilkent University, Ankara
### *Teaching Assistant*

**2017-2021**

- 3 years as a teaching assistant at EEE493-494 Industrial Design Projects course. Worked together with many companies and oversaw many projects. 

[Fair Web Page (In Turkish)](http://ee.bilkent.edu.tr/fuar/2018/index.html) | [Course Video](https://youtu.be/gbnNAyY2izk)

- 1 year experience as a Teaching Assistant at Digital Electronics and Computational Neuroscience.

# Work Experience

-----------------------------------------------------------
## UMRAM (National Magnetic Resonance Research Center), Ankara
### *Internship*

**June-August 2016**

[UMRAM Website](http://umram.bilkent.edu.tr/)

-----------------------------------------------------------
# NanoMagnetics Instruments, Ankara
## *Internship*

**June-August 2015**

[NanoMagnetics Instruments Website](https://www.nanomagnetics-inst.com/)