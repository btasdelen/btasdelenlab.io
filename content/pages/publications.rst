Publications
##############

:date: 2020-05-30 10:20
:tags: publications
:slug: publications
:authors: Bilal Taşdelen
:summary: My Publications


Journal Publications
--------------------------------------------------------------------

.. bibliography:: /bibliography/JournalPapers.bib
    :template: bibliography

Patents
--------------------------------------------------------------------

.. bibliography:: /bibliography/Patents.bib
    :template: bibliography

Conference Abstracts
--------------------------------------------------------------------

.. bibliography:: /bibliography/ConferenceAbstracts.bib
    :template: bibliography


**Presentations**

* **Oral Presentation**, *International Society for Magnetic Resonance in Medicine*, 2023, Singapore, Singapore. **Summa Cum Laude (Top 5%)**

* **Oral Presentation**, *International Society for Magnetic Resonance in Medicine*, 2023, Singapore, Singapore. **Summa Cum Laude (Top 5%)**

* **Invited Talk**, *National Heart, Lung and Blood Institute, National Institute of Health*, 2022, Bethesda, Maryland, USA

* **Poster Presentation**, *International Society for Magnetic Resonance in Medicine*, 2022, London, UK

* **Poster Presentation**, *International Society for Magnetic Resonance in Medicine*, 2021, Online

* **Poster Presentation**, *International Society for Magnetic Resonance in Medicine*, 2020, Online

* **Poster Presentation**,  *International Workshop on Magnetic Particle Imaging*, 2020, Online

* **Oral Presentation**, *TURKRAD (Turkish Radiologists Society) 2019*, Antalya, Turkey.

* **Poster Presentation**, *International Society for Magnetic Resonance in Medicine*, 2019, Montreal, Canada.

* **Oral Presentation**, *Graduate Research Conference 2019*, Ankara, Turkey.

* **Poster Presentation**, *Graduate Research Conference 2018*, Ankara, Turkey.
