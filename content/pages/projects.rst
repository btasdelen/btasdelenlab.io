Projects
##############

:date: 2020-05-30 10:20
:tags: projects
:slug: projects
:authors: Bilal Taşdelen
:summary: My Projects

----------------------------

**PhD Project:** *'High-amplitude Pilot Tone for 0.55T MRI'*
--------------------------------------------------------------------

Working on techniques that makes it possible to use pilot-tone for cardiac MRI at 0.55T.

----------------------------

**PhD Project:** *'Electromagnetic Interference Mitigation for MRI'*
--------------------------------------------------------------------

Working on EMI mitigation techniques with an emphasis on in room devices such as life-support, stress/exercise and pilot tone devices. 

----------------------------


**PhD Project:** *'Spiral Fat-Water Separated Imaging at 0.55T'*
--------------------------------------------------------------------

Exploring high resolution fat-water imaging at mid field MRI machines by using time-efficient sampling strategies.

----------------------------

**MS Project:** *'Simultaneous Transmission and Reception in MRI'*
--------------------------------------------------------------------

Master's thesis. Designing a system that is able to actively isolate coupled transmit and receive coils in the case that transmission and reception happens simultaneously. Summary of the work so far:
Designed and tested many RF devices including (not limited to): vector modulator, low noise pre-amplifier, high gain MMIC amplifier... Developed a LUT based algorithm [3]. Implemented SWIFT, cSWIFT and UTE pulse sequences for the Siemens Tim Trio MRI Scanner (C++). Noise and distortion analysis for simultaneous transmission and reception[2].

----------------------------

**MS Project:** *'Vector Modulator Based Active Compensation of Direct Feedthrough in MPI'*
-----------------------------------------------------------------------------------------------

Designed and implemented a vector modulator for Magnetic Particle Imaging to eliminate direct feedthrough and shown its application.

----------------------------

**MS Project:** *'Spatiotemporal magnetic field monitoring with hall effect sensors during the MRI'*
----------------------------------------------------------------------------------------------------------------------------------------
Worked in the development of a magnetic field monitoring system based on hall effect sensors for MRI. We have a patent application from this project.

----------------------------

**Senior Project:** *'Target and Self Localization via UAV Camera'*
--------------------------------------------------------------------

This is a group project in collaboration with ASELSAN. In this project, I was involved with data acquisition from UAV sensors and camera, and integration of these sensors into ORB-SLAM 2 algorithm.

**Project Video (English Subtitles)**

.. youtube:: LewDRp9TAZI 

**Supplementary Video 1**

.. youtube:: c33mAm_vG0c 

**Supplementary Video 2**

.. youtube:: 34y-dVIXxdM

**Awarded 1st Place Prize, IEEE Turkey Senior Projects Competition, 2017.**

----------------------------

**3rd year Internship Project:** *'MR Elastography'*
--------------------------------------------------------------------

Implemented k-MDEV algorithm in MATLAB. Designed an experiment setup that accelerates MRE experiments. As a result, I co-authored 3 abstracts [5-7] and 1 work in progress paper.
