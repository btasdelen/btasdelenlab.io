Title: Education
Date: 2020-05-30 10:20
Modified: 2010-12-05 19:30
Tags: education
Slug: education
Authors: Bilal Taşdelen
Summary: My Education History

--------------------------------------------------

University of Southern California, Los Angeles
=========================================================

**PhD. Department of Electrical and Computer Engineering**
---------------------------------------------------------

**2021-ongoing**

CGPA: 4.00/4.00

--------------------------------------------------

Bilkent University, Ankara
=========================================================

**MS. Department of Electrical and Electronics Engineering**
---------------------------------------------------------

**2017-2020**

CGPA: 3.67/4.00

--------------------------------------------------
Bilkent University, Ankara
=========================================================

**B.Sc. Department of Electrical and Electronics Engineering**
----------------------------------------------------------

**2013-2017**

CGPA: 3.53/4.00

--------------------------------------------------

AGH University of Science and Technology, Krakow
=========================================================

**Faculty of Computer Science, Electronics and Telecommunications, *ERASMUS***
------------------------------------------------------------------------------------
**2015-2016**

--------------------------------------------------

